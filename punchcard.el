;;; punchcard.el --- Manage split file init and their inter-dependencies -*- lexical-binding: t -*-

;; Copyright (C) 2022  Guillaume "Gene" Pasquet.

;; Author: Gene Pasquet <dev@etenil.net>
;; URL: https://gitlab.com/binary-ec/punchcard
;; Version: 0.1
;; Package-Requires: ((emacs "27.1"))

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; Load up all split config files from one directory.
;;
;; Punchcard manages only flat lists of configuration files (no nesting)
;; and will only run those files after they are loaded and their
;; dependency chain resolved. For this reason, evaluating a config file
;; written with Punchcard will have no effect.
;;
;; Example of a config file written with Punchcard:
;;
;;     (punchcard
;;      '("packages")
;;      (add-hook my-mode-hook #'something-provided-by-packages))
;;
;; The following commands are supplied to help manage configurations:
;; - `punchcard-load' :: Load up all config files that exist in the
;;   directory supplied as the argument to this command
;;
;; - `punchcard-load-file' :: Load up the file in the active buffer
;;   unconditionally. Does not perform dependency checks.
;;
;; - `punchcard-find-file' :: Find a configuration file, or create
;;   a new one.
;;
;; - `punchcard-display-dependencies' :: Display the list of config
;;   files, their loading order and their dependencies. Mostly used
;;   for diagnostics or debugging.

;;; Code:

(require 'cl-lib)

;; Global variables
(defvar punchcard-conf-dir nil
  "The path to the folder that contains the split config files")

(defvar punchcard-confs '()
  "List of punchcards config modules")
(defvar punchcard--loading nil
  "The punchcard module being currently loaded")

;; The conf file structure
(cl-defstruct punchcard-conf
  "Struct representation of a punchcard configuration file"
  name deps callback)

(defun punchcard-conf-summary (conf)
  "Return a conf summary as a nice display string"
  (list (punchcard-conf-name conf)
        (punchcard-conf-deps conf)))

;; The punchcard macro
(defmacro punchcard (depends &rest config)
  "Define a config module.

The name of the module is the current value of PUNCHCARD--LOADING
and the dependencies DEPENDS and body CONFIG.

DEPENDS must be either a list of strings, or nil"
  `(punchcard-register (make-punchcard-conf :name punchcard--loading
                                            :deps ,depends
                                            :callback (lambda () ,@config))))


(defun punchcard-register (conf)
  (setq punchcard-confs (if punchcard-confs
                            (cons conf punchcard-confs)
                          (list conf))))


(defun punchcard--conf-sorter (a b)
  "Compare two confs in order to sort them."
  (let ((deps-a (punchcard-conf-deps a))
        (deps-b (punchcard-conf-deps b)))
    (cond ((not deps-a) t)
          ((not deps-b) nil)
          ((member (punchcard-conf-name a) deps-b) t)
          ((member (punchcard-conf-name b) deps-a) nil)
          (t (< (length deps-a) (length deps-b))))))

(ert-deftest punchcard--sort-test1 ()
  "Test sorting confs"
  (should (equal t
                 (punchcard--conf-sorter
                  (make-punchcard-conf :name "a" :deps nil :callback nil)
                  (make-punchcard-conf :name "b" :deps '("a") :callback nil))))
  (should (equal nil
                 (punchcard--conf-sorter
                  (make-punchcard-conf :name "a" :deps '("b") :callback nil)
                  (make-punchcard-conf :name "b" :deps nil :callback nil))))
  (should (equal t
                 (punchcard--conf-sorter
                  (make-punchcard-conf :name "a" :deps '("c") :callback nil)
                  (make-punchcard-conf :name "b" :deps '("a") :callback nil))))
  (should (equal nil
                 (punchcard--conf-sorter
                  (make-punchcard-conf :name "a" :deps '("b") :callback nil)
                  (make-punchcard-conf :name "b" :deps '("c") :callback nil)))))

(defun punchcard-load (conf-dir)
  "Load all configuration files in CONF-DIR, and set
PUNCHCARD-CONF-DIR to CONF-DIR."
  (interactive "DBase config directory ")
  ;; Ensure the punchcard-conf-dir is set
  (unless punchcard-conf-dir
    (setq punchcard-conf-dir conf-dir))
  
  ;; Reset confs
  (setq punchcard-confs '())
  
  ;; Collect all punchcards
  (dolist (file (directory-files conf-dir t "\.el$"))
    (progn
      (setq punchcard--loading (file-name-base file))
      (load file)))
  ;; Solve dependencies
  (setq punchcard-confs (sort punchcard-confs #'punchcard--conf-sorter))

  ;; Actually load the configuration
  (dolist (conf punchcard-confs)
    (funcall (punchcard-conf-callback conf))))

;; Convenience functions
(defun punchcard--get-conf (name)
  (car (cl-remove-if-not
        (lambda (conf) (string= (punchcard-conf-name conf) name))
        punchcard-confs)))

(defun punchcard--do-load-file ()
  (setq punchcard--loading "--temp")
  (eval-buffer)
  (funcall (punchcard-conf-callback (punchcard--get-conf "--temp")))
  (setq punchcard-confs (cl-remove-if
                         (lambda (conf) (string= (punchcard-conf-name conf) "--temp"))
                         punchcard-confs)))

(defun punchcard--current-buffer-conf-module-p ()
  (save-excursion
    (save-match-data
      (goto-char (point-min))
      (search-forward "(punchcard" nil t))))

(defun punchcard-load-file ()
  "Load the punchcard configuration in the currently open file."
  (interactive)
  (if (punchcard--current-buffer-conf-module-p)
      (punchcard--do-load-file)
    (error "File isn't a punchcard configuration module")))

(defun punchcard-find-file ()
  "Open a config file from the config directory."
  (interactive)
  (let* ((filename (completing-read "Configuration " (directory-files punchcard-conf-dir nil "\.el$")))
         (filepath (file-name-concat punchcard-conf-dir filename)))
    (find-file (file-name-concat punchcard-conf-dir filename))
    (unless (file-exists-p filepath)
      (insert "(punchcard\n nil\n\n )\n"))))

(defun punchcard-display-dependencies ()
  "Display the list of configuration modules in loading
order in a temporary buffer."
  (interactive)
  (let ((buffer (get-buffer-create "*Punchcard*")))
    (with-current-buffer "*Punchcard*"
      (read-only-mode -1)
      (erase-buffer)
      (insert (format "%s" (mapcar #'punchcard-conf-summary punchcard-confs)))
      (lisp-data-mode)
      (replace-string "(" "\n(" nil 0 (buffer-end 1))
      (beginning-of-buffer)
      (kill-line)
      (indent-region 0 (buffer-end 1))
      (read-only-mode))
    (display-buffer (get-buffer "*Punchcard*"))))

(provide 'punchcard)
;;; punchcard.el ends here
