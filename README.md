PUNCHCARD
=========

Punchcard is a split Emacs configuration manager.

## Installation

Copy the `punchcard.el` file somewhere in your Emacs load path, then
require it. In the following example, the file `punchcard.el` was
saved in the folder `~/.emacs.d/elisp`.

```elisp
(add-to-list 'load-path "~/.emacs.d/elisp" t)
(require 'punchcard)
```

## Usage

Punchcard is a tool to allow breaking up emacs init files into smaller
chunks and manage their dependency load-order. All init segment files
must reside in the same folder, for example `~/.emacs.d/init`. Those
config files are elisp files wrapped into the `punchcard` macro. For
example:

```elisp
(punchcard
  '("packages" "yasnippet")
  
  (add-hook 'python-mode-hook #'yas-blah))
```

Here we are defining some python-specific settings within the
`punchcard` body. The line `'("packages" "yasnippet")` defines the
configuration dependencies for this file. Punchcard will ensure the
files `packages.el` and `yasnippet.el` are loaded before loading the
current file.

## Loading a configuration file interactively

When editing a configuration file, evaluating it with `M-x
eval-buffer` will not produce any effect. This is because the config
code within the `punchcard` body is executed later. To facilitate
working on configuration files, use the function `M-x
punchcard-load-file` instead.

!! NOTE: at this time, `punchcard-load-file` does not handle dependencies.
